//
//  ContentView.swift
//  AnimeIOS
//
//  Created by Alumnos on 11/19/19.
//  Copyright © 2019 dash. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            TabView{
                ListaSimpleAnime().tabItem() {
                    Image("Anime1")
                    Text("Lista S")
                }
                ListaImagenAnime().tabItem(){
                    Image("Anime1")
                    Text("Lista C")
                }
            }.navigationBarTitle(Text("Animes Chidos"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
