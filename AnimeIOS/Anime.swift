//
//  Anime.swift
//  AnimeIOS
//
//  Created by Alumnos on 11/19/19.
//  Copyright © 2019 dash. All rights reserved.
//

import Foundation

struct Anime: Codable {
    let _id: String
    let nombre: String
    let descripcion: String
    let imagen: String
    let link: String

}
