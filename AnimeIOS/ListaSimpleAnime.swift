//
//  ListaSimpleAnime.swift
//  AnimeIOS
//
//  Created by Alumnos on 11/19/19.
//  Copyright © 2019 dash. All rights reserved.
//

import SwiftUI

struct ListaSimpleAnime: View {
    @ObservedObject private var animeListVM = AnimeListViewModel()
    
    var body: some View {
        List(self.animeListVM.animes, id: \.nombre){
                anime in
                Text(anime.nombre)
        
        }
    }
}

struct ListaSimpleAnime_Previews: PreviewProvider {
    static var previews: some View {
        ListaSimpleAnime()
    }
}
