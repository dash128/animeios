//
//  AnimeListViewModel.swift
//  AnimeIOS
//
//  Created by Alumnos on 11/19/19.
//  Copyright © 2019 dash. All rights reserved.
//

import Foundation

class AnimeListViewModel: ObservableObject {
    @Published var animes = [AnimeViewModel]()

    init(){
        WebService().getDatos {
            animes in
            
            if let animes = animes{
                self.animes = animes.map(AnimeViewModel.init)
            }
            
            // self.animes = animes.map(AnimeViewModel.init)
        }
    }
}

struct AnimeViewModel {
    var anime: Anime

    init(anime: Anime){
        self.anime = anime
    }

    
    var nombre: String{
        return self.anime.nombre
    }

    var descripcion: String {
        return self.anime.descripcion
    }
}

