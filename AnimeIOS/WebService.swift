//
//  WebService.swift
//  AnimeIOS
//
//  Created by Alumnos on 11/19/19.
//  Copyright © 2019 dash. All rights reserved.
//

import Foundation

class WebService {
    func getDatos(completion: @escaping ([Anime]?) -> () ){
        guard let url = URL(string: "https://anime-dash.herokuapp.com/api/anime") else {
            fatalError("Invalid URL")
        }

        URLSession.shared.dataTask(with: url ){ data, response, error in
            guard let data = data, error == nil else{
                
                completion(nil)
                if let error = error {
                  print(error.localizedDescription)
                }
                return
                
            }

            let datos = try? JSONDecoder().decode( [Anime].self , from: data)

            DispatchQueue.main.async{
                completion(datos)
            }
            
            if let error = error {
              print(error.localizedDescription)
            }

        }.resume()
    }
    
}
